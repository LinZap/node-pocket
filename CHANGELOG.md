# Change Log

這裡會記錄近幾版修改的細節與說明


## V3.2
v3.2 將帶你進入全面的 ES6 世界!

### New - 全新的初始歡迎頁面
原先的初始化面太醜，現已經改為較正式的歡迎畫面。

<br>

### New - Quick-Render 預設改為 replace 模式
當你使用 Quick-Render 模式時，會**直接取代**掉原先的 `tar`  
使得你可以撰寫更加直覺的 EJS Template  
範例：  
`ejs`
```html
<!-- 標籤名稱怎麼定都無所謂，反正最後都會被取代 -->
<div class="page">
    <MyBox ejs="view/mybox"></MyBox>
</div>
```
`view/mybox.ejs`
```html
<div class="mybox">MyBox</div>
```
最後 render 結果
```html
<div class="page">
    <div class="mybox">MyBox</div>
</div>
```

<br>

### New - i3s.render 加入 replace 參數 
同上述，現在允許你自由選擇是否取代掉父元素 (預設 `false`)  
注意！無法與 `append` 與 `prepaned` 參數並用
```js
i3s.render({
    ejs: 'view/mybox',
    tar,
    replace: true
})
```

<br>

### New - i3s.render 加入 tar 參數 
現在允許 `render` 的設定參數 `target` 相等於 `tar`，如上範例

<br>

### New - 全新的 SPA 開發環境
我們為使用者預設安裝 [Page.js](https://github.com/visionmedia/page.js) Bower Component  並且在 `webconfig.json` 中加入 `SPA` 的項目。  
你只需要修改 `routing: true` 就會自動載入 `page.js`  
設定 `base: 'mybase' ` 就會自動設定 `<base href="/mybase/">`  
若你用的是開發 server，還會自動將 URL 全部導向到 `index.html`  
且新增一個新的 API `i3s.base("base")` 幫助使用 `page.js` 讀取並設定 `webconfig.json` 中的設定

其他使用上的細節可以參閱官方文件中的 [進階](https://linzap.gitbooks.io/i3s-awesome/content/advance.html) 案例說明。

<br>

### New - 全面 ES6 化
在專案中的各處，都與上版 `v3.1` 的語法都有些不同，主要是將 ES5 的語法改為 ES6  
你可能會直接感受到 `app/js/UI.js`  `app/js/app.js` 的變化。

<br>

且全面支援 ES6 語法，與部分的 ES7 語法，下面列出幾種常見的案例：  
```js
import ... from 'module'
export ...;
var q = ()=>{} 
var {a,b} = obj
var n = {...obj1,...obj2}
function* generator(){}
function fun(arg,...arg2)
```
注意！第一次 build 會比較久是因為在 `vender` 中新引入了 `babel-core/register` 與 `babel-polyfil` 來轉譯 ES6 語法。 

<br>

### New - 新增內建模組 Async
這個模組能夠幫你大幅減少「同步」功能的撰寫複雜度
```js
import {async,addTask} from './../lib/async';
function StepByStep(next,data=1){
	setTimeout(()=>{
		console.log(data);
		next(++data)
	},1000)
}
for(var i=0;i<=3;i++) addTask(StepByStep)
async()
// 1
// 2
// 3
```

<br>

### Fix - 部分 ES6 語法造成執行階段出錯
移除了還有嚴重 bug 的 `gulp-babel` 模組，並加入了 `babelify` 來修正這個問題。

<br>

### Remove - 移除預設的 CSS,JS,EJS,BowerComponents,Packages 等無用內容
上個版本中有過多的預設參考檔案，在這個版本中已經全數移除，留給開發者一個乾淨初始環境

<br>

### Fix - Core library 模組化
修正 Core library 尚未模組化的問題，現在已經有更完善的擴充方式
