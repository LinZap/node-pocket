var through = require('through2'),
    gutil = require('gulp-util'),
    fs = require("fs");

module.exports = function (opttion) {

    /*
        @file filesource (buffer[])
        @enc encode(utf8)
        @callback function(er, data){ afterTransform(stream, er, data); }
    */
    var opt = Object.assign({ dist: "dist/per-ejs.json", enc: "utf8" }, opttion),
        ejs = {};

    return through.obj({highWaterMark:100},function (file, enc, cb) {
        console.log(file.path);
        var p = file.path.slice(__dirname.length)
        .replace(/\\/g,"/")
        .replace(/\/ejs\//g,"")
        .replace(/.ejs$/g,"");

        ejs[p] = file.contents.toString();
        fs.writeFileSync(opt.dist, JSON.stringify(ejs),opt.enc);
        //this.push(file);
        return cb(null,file);

    });
};