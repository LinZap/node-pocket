'use strict';

var fs = require('fs'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    gulp = require('gulp'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    gutil = require('gulp-util'),
    stylus = require('gulp-stylus'),
    livereload = require('gulp-livereload'),
    opn = require('opn'),
    express = require('express'),
    app = express(),
    config = require('./webconfig.json'),
    preEJS = require("./pre-ejs");

const debug = config['debug'];
const port = config['serverPort'];
const dist = './dist';
const dist_js = dist+'/js';
const dist_css = dist+'/css';
const dist_img = dist+'/img';
const routing = config["SPA"]["routing"];
var base = config["SPA"]["base"];


[dist,dist_js,dist_css,dist_img].forEach(function(path){
    try { fs.mkdirSync(path); }
    catch(err) {  console.log(`${path} already exist`); }
});

gulp.task('js',['pre-ejs'], function () {

    var b = browserify({
        entries: './app/js/app.js',
        debug: debug
    })
    .transform("babelify", {
        presets: ['es2015','stage-0'],
        plugins: ['transform-object-rest-spread']
    });

    return b.bundle()
    .pipe(source('./app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: debug}))
    .pipe(uglify())
    .on('error', gutil.log)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(dist_js))
    //.pipe(livereload());
});


gulp.task('stylus', function () {
    return gulp.src('./app/css/main.styl')
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: debug}))
    .pipe(stylus({compress: !debug}))
    .on('error', gutil.log)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(dist_css))
    .pipe(livereload());
});



gulp.task('vender', function () {

    var b = browserify({
        entries: './app/vender/entry.js',
        debug: debug
    });

    return b.bundle()
    .pipe(source('./vender.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: debug}))
    .pipe(uglify())
    .on('error', gutil.log)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(dist_js))
    .pipe(livereload());
});


gulp.task('html', function () {
    return gulp.src('./app/index.html')
    .pipe(gulp.dest(dist))
    .pipe(livereload());
});

gulp.task('pre-ejs', function () {
    return gulp.src('ejs/**/*.ejs')
    .pipe( preEJS() )
});



gulp.task('watch',['html','stylus','vender','js'],function(){

    livereload.listen({ start: debug });

    // index.html
    gulp.watch(['./app/index.html'], ['html']);
    // Vender
    gulp.watch(['./app/vender/*.js'], ['vender']);
    // JS
    gulp.watch(['./app/js/**/*.js','./app/lib/**/*.js'], ['js']);
    // Stylus
    gulp.watch(['app/css/*.styl'], ['stylus']);

    // preEJS
    gulp.watch(['ejs/**/*.ejs'], ['js']);

    gulp.watch('dist/css/main.css').on('change', livereload.changed);
    gulp.watch('dist/js/app.js').on('change', livereload.changed);
    gulp.watch('dist/js/vender.js').on('change', livereload.changed);
    gulp.watch('dist/index.html').on('change', livereload.changed);

    
});


gulp.task('server',['watch'],function(){
    app.use('/bower_components',express.static('bower_components'));
    app.use('/js',express.static('dist/js'));
    app.use('/css',express.static('dist/css'));
    // app.use('/',express.static('dist'));
    //app.use('/ejs',express.static('ejs'));
    // start webserver and browser

    if(base.length) base+= "/";

    const baseJS = routing? '<script src="bower_components/page/page.js"></script>': '',
          baseTag = routing? `<base href="/${base}">` : '<base href="/">';

    app.get('/*', function (req, res) { 
        var html = fs.readFileSync("./dist/index.html", 'utf8');
        html = html.replace('<base></base>',baseTag);
        html = html.replace('<pageJS></pageJS>',baseJS);
        res.send(html);
    });

    app.listen(port,function(){
        console.log(`app listening on port ${port}`);
        opn(`http://localhost:${port}`);  
        livereload();
    });
})


gulp.task('default', ['server']);