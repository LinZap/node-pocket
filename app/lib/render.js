import preEJS from './../../dist/per-ejs.json'

function render(set){
	if(!preEJS[set.ejs]){
		console.error(`Can not find EJS: ${preEJS[set.ejs]}`);
		return false;
	}

	var code = new EJS({text: preEJS[set.ejs]}).render(set.data),
		ele = $('<div>').append(code).children(),
		tar = set.tar || set.target;

	if(tar){
		if(typeof tar=='string') tar=$(`#${tar}`);
		
		if(set.replace){
			tar.after(ele).remove();
		}
		else{
			if(set.append) tar.append(ele);
			else if(set.prepend) {tar.prepend(ele);}
			else tar.html(ele);
		}
		
		ele.find('[ejs]').each((index,el)=>{quickRender($(el))});
		ele.filter('[ejs]').each((index,el)=>{quickRender($(el))});
		ele.removeAttr('ejs');
	}
	return ele;
}

function quickRender(el){
	var ejs_fun = el.attr('ejs');
	if(ejs_fun.indexOf("@")>-1) window.UI[ejs_fun.slice(1,ejs_fun.length)](el)
	else render({ejs:ejs_fun,target:el,replace:true})
	el.removeAttr('ejs')

}

function layout(layout="sample"){
	return render({
		ejs: "layout/"+layout,
		target: "section"
	})
}

function title(str=""){
	document.title = str
}

export {
	render,
	quickRender,
	layout,
	title
}