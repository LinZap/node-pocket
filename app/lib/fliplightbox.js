/*
flipbox plug-in
@Zap
*/
module.exports = {

	init: function(){
		
		$('body').delegate('.cover', 'animationend webkitAnimationEnd oanimationend MSAnimationEnd', function(event) {
			var cover = $(this),
			flipbox = $(this).children('.flipbox'),
			body = $('body');
			flipbox.addClass('flip').click(function(event) {
				event.stopPropagation();
			});
			body.addClass('noscroll');
			cover.click(function(event) {
				flipbox.on('webkitTransitionEnd transitionend oTransitionEnd', function(event) {
					cover.remove();
				});
				flipbox.removeClass('flip');
				body.removeClass('noscroll');
			});
		});
	},

	open: function(){

		var flipbox = $('<div>').addClass('flipbox'),
		cover = $('<div>').addClass('cover').append(flipbox);
		flipbox.data('offset','0');
		$('body').append(cover);
		return flipbox;
	}

};
