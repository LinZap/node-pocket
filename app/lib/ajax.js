import config from './../../webconfig.json'

export function api(){
	var apipath,set,callback;

	if(arguments.length>2){
		apipath = arguments[0];
		set = arguments[1];
		callback = arguments[2];
	}
	else{
		apipath = config["quickapi"];
		set = arguments[0];
		callback = arguments[1];
	}

	var ajax = $.ajax({url: apipath , dataType: 'json', data: set})
	.done(function(data) { if(callback)callback(data); })
	.fail(function(data){ console.log(set,data); });
	return ajax.promise();
}

export function jsonfeed(){

	var apipath,set,callback;

	if(arguments.length>2){
		apipath = arguments[0];
		set = arguments[1];
		callback = arguments[2];
	}
	else{
		apipath = config["jsonfeed"];
		set = arguments[0];
		callback = arguments[1];
	}

	var ajax = $.ajax({url: apipath ,dataType: 'json', data: set})
	.done(function(data) { if(callback)callback(data); })
	.fail(function(data){ console.log(set,data); });
	return ajax.promise();
}