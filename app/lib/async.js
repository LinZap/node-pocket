/*
	async function
	by Zap
*/
var Tasks = [];

export function addTask(fun){
	if(typeof(fun)=='function') Tasks.push(fun)
}

export function async(){
	var lastReturn,
		gen = (function* (){
			for(let i=0;i<Tasks.length;i++)
				if(typeof(Tasks[i])=='function'){
					lastReturn = yield Tasks[i](gen.next.bind(gen),lastReturn)
				}
				else continue
		})()
	gen.next(lastReturn)
	return gen
}
