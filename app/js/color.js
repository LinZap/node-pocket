export function rgb(hex,a) {
    var result = /^#?([a-fA-F\d]{2})([a-fA-F\d]{2})([a-fA-F\d]{2})$/i.exec(hex) || 
    /^#?([a-fA-F\d]{1})([a-fA-F\d]{1})([a-fA-F\d]{1})$/i.exec(hex),
    	r = result? parseInt(result[1], 16):0,
    	g = result? parseInt(result[2], 16):0,
    	b = result? parseInt(result[3], 16):0;
    return a? `rgba(${r},${g},${b},${a})` : `rgb(${r},${g},${b})`;
}

