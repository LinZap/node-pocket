export function genArr(start=0,end=0,gap=1){
	var arr = [];
	for (var i = start; i <= end ;i+=gap) arr.push(i);
	return arr;
}

export function fillArr(num,getVal){
	var arr = [];
	if(typeof(getVal)=='function')
		for (var i = 0; i < num; i++) arr.push(getVal(i));
	else
		for (var i = 0; i < num; i++) arr.push(getVal);
	return arr;
}


export function addArr(...arrs){

	if(!arrs.length) return [];

	var len = arrs[0].length,
		ans = [...arrs[0]]

	for (var i = 1; i < arrs.length; i++) {
		if(len != arrs[i].length) {
			console.error("can not add arrs");
			return [];
		}
		for (var j = 0; j < arrs[i].length; j++) ans[j] += arrs[i][j];
	}

	return ans;
	
}


export function numArr(arr=[]){
	var count = 0;
	if( Array.isArray(arr) ) 
		return arr.length;
	else for (var k in arr) 
		count++;
	return count;
}

export function getObj(obj={},idx=0){
	var i=0;
	for (var k in obj) {
		if(idx==i) return [k,obj[k]];
		i++;
	}
	return null;
}