import * as i3s from './../lib/i3s'
import * as UI from './UI'
import {fillArr,addArr,numArr,getObj,genArr} from './Array'
import {rand} from './Math'
import {marker, surface, figure} from './Plot'
import {rgb} from './color'

window.UI = UI

i3s.title('Pocket');
i3s.layout('pocket')

const potinNum = 200

var A = {
		x: fillArr(potinNum, i=>2+rand(-4,4)),
		y: fillArr(potinNum, i=>2+rand(-4,4)),
		z: fillArr(potinNum, i=>2+rand(-4,4)),
		color:rgb('#4A6DF9',0.9)

	},
	B = {
		x: fillArr(potinNum, i=>-2+rand(-4,4)),
		y: fillArr(potinNum, i=>-2+rand(-4,4)),
		z: fillArr(potinNum, i=>-2+rand(-4,4)),
		symbol: 'x',
		color: rgb('#BB0000',0.9)
	}

var AB = {
		x: [...A.x ,...B.x],
		y: [...A.y ,...B.y],
		z: [...A.z ,...B.z]
	},

	Sign = [...fillArr(potinNum,1), ...fillArr(potinNum,-1)],

	W = { a: 1, b: 1, c: 1, d: 1 },
	bestW = {...W},

	min_err = numArr(Sign) + 1,
	num_err = 1,
	iteration = 1,
	until = 2000,
	fix_num = 0,

	suf = {
		x: genArr(-8,8,0.5),
		y: genArr(-8,8,0.5),
		...bestW
	}

var fig = figure(_('res'))
		  .plot(marker(A))
		  .plot(marker(B))
		  .plot( surface(suf) )


function pocket(){

	setTimeout(()=>{
		if(num_err > 0 && iteration<until){

			var err_set = NonAtRigtSide(W,AB,Sign);

			num_err = numArr(err_set)

			if(num_err<=0) return;

			// 隨機抽一個錯的
		    let rand_seed = Math.floor(rand()*num_err)
		   
		    var err_info = getObj(err_set,rand_seed),
		    	err_idx = err_info? err_info[0]: console.error(err_set,err_info,rand_seed),
		    	err_point = {
		    		x: AB.x[err_idx],
		    		y: AB.y[err_idx],
		    		z: AB.z[err_idx]
		    	}

		    W = fixError(W,err_point,Sign[err_idx])
		    

		    if(num_err < min_err){

		    	

				bestW = {...W}
				min_err = num_err

				suf = {
					x: genArr(-8,8,0.5),
					y: genArr(-8,8,0.5),
					...bestW
				}

				fig.remove(2).plot( surface(suf) )
				fix_num++
				ui_fun(bestW)
				ui_fixnum(fix_num)
		    }

		    iteration++
		    ui_iter(iteration,until)
		    ui_points(min_err,num_err)
		    pocket()
		}
	},100)
}






function NonAtRigtSide({a,b,c,d},{x,y,z},ans){
	var hash = {};
	for (var i = 0; i < x.length; i++) {
		let res = Math.sign(a*x[i]+b*y[i]+c*z[i]+d)-ans[i];
		if(res!=0) hash[i] = res;
	}
	return hash;
}

 // 修正 w = w + Y(err_index(rand_seed))*Xext(err_index(rand_seed), :);
function fixError({a,b,c,d},{x,y,z},sign){
	return {
		a: a+ sign*x,
		b: b+ sign*y,
		c: c+ sign*z,
		d: d+ sign
	};
}



/*
	----------- console -----------
*/
var _a = _('_a'),
	_b = _('_b'),
	_c = _('_c'),
	_d = _('_d'),
	_run = _('run'),
	_iter = _('_iter'),
	_limit = _('_limit'),
	_better = _('_better'),
	_now = _('_now'),
	_fixnum = _('_fixnum')

ui_fun(bestW)
ui_iter(iteration-1,until)
ui_points('None','None')
ui_fixnum(0)

function ui_fun({a,b,c,d}){
	_a.innerHTML = a
	_b.innerHTML = b
	_c.innerHTML = c
	_d.innerHTML = d
}

function ui_iter(iter,limit){
	_iter.innerHTML = iter
	_limit.innerHTML = limit
}

function ui_points(better,now) {
	_better.innerHTML = better
	_now.innerHTML = now
}


function ui_fixnum(num){
	_fixnum.innerHTML = num
}

_run.addEventListener("click", function(){
	this.setAttribute("disabled", "disabled")
    pocket()
});

function _(id){
	return document.getElementById(id);
}

