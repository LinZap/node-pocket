import {surface as suf} from './Surface'


export function marker({x,y,z,symbol='circle',color='#000'}){
	return {
		x,y,z,
		mode: 'markers',
		marker: {
			color,symbol,
			size: 3.5,
			opacity: 0.9
		},
		type: 'scatter3d'
	}
}


export function surface(set={x:[],y:[],a:0,b:0,c:0,d:0}){
	return Object.assign(suf(set),{
		type: 'surface',
		showscale: false
	})
}


export function figure(tar,set={}){
	var layout = Object.assign({title:'Plot',autosize:true,margin:{l:'auto',r:'auto',b:10,t:10},height:450},set),
		chain = {
			plot: function(trace){
				Plotly.addTraces(tar, trace)
				return chain
			},
			remove: function(idx){
				Plotly.deleteTraces(tar, idx)
				return chain
			},
			update: function(obj,idx){
				Plotly.restyle(tar, obj, idx)
				return chain
			}
		}
	Plotly.newPlot(tar, [] , layout);
	return chain
}

