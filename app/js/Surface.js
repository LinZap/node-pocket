//ax+by+cz+d=0
// z = -(ax+by+d)/c
// a = 3.70145452754344	
// b = 10.3844801712330
// c = 4.62691121095473	
// d = 3

export function surface({x=[],y=[],a=0,b=0,c=0,d=0}){
	var z = [];
	for (var i = 0; i < x.length; i++) {
		var sub_arr = [];
		for (var j = 0; j < y.length; j++) 
			sub_arr.push( -1*( a*x[i] + b*y[j] + d )/c  )
		z.push(sub_arr)
	}
	return {x,y,z};
}