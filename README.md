# I3S-Awesome

## [官方說明](http://163.22.21.72/pocket/)

## Installation

install gulp.js
````
npm install -g gulp
````

install packages
````
npm install
````

install components
````
bower install
````

## Usage
launch
````
gulp
````

# License : MPL 
https://gitlab.com/wkelab/i3s-awesome/blob/master/LICENSE